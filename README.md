# Agile-Development-Methodologies

Agile Software Development is a set of software development methods in which requirements and solutions evolve through collaboration between self-organizing, cross-functional teams.

### 1. Agile Fundamentals 

#### intended for

  - Team members starting out in Agile projects
  - Project managers entering the Agile environment
  - Managers of teams involved in Agile development
  - Consultants looking for a solid grounding in Agile software development

#### Contents

 - The genesis of Agile - where these approaches came from and why they work
 - The Agile lifecycle and iterations
 - Roles on an Agile project
 - The phases of an Agile project
 - Project initiation activities - making sure we start right, focus on value and build the right product
 - User Stories:
   - What's a story?
   - How big is a story?
   - Identifying stories
   - Characteristics and content of stories
   - What does “Done, Done, DONE” mean?
   - Quality stories
   - Stories and Epics
   - Estimating from stories
   - Release planning
   - Acceptance tests and verifying stories
   - Elaborating stories to be useful without wasting time or effort
 - Agile tools - big visible charts, things on walls, velocity, burn-up and burn-down
 - The “pulse” of an Agile project:
   - Iteration planning
   - Collaborative work
   - Make flow visible with the story wall
   - Daily standup
   - Showcase
   - Retrospective
 - Agile without iterations - Kanban flow
 - Supporting tools
 - Testing on Agile projects
 - Design and development practices in an Agile setting - TDD, continuous integration, refactoring, pair programming, simple design
 - Project leadership roles and responsibilities, how to nurture self-organisation
 - Working effectively in empowered teams
 - Listening and collaboration skills
 - Dealing with issues and conflict in the team
 - Where to from here?

###2. Adaptive Portfolio Management
3. Agile Product OwnerShip
4. Adaptive Leadership
5. Agile Facilitation and Iteration Management
6. Agile Coaching
7. The Agile Business Analyst
8. Agile Programming Techniques
9. Agile Design and Architechture
10. Agile Test Automation
11. Introduction To Agile
12. Agile For Executives
13. Business Agility Accelarator
14. Agile Testing
